package it.unibo.oop.lab.lambda.ex02;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static java.util.stream.Collectors.*;

import java.util.Arrays;

/**
 *
 */
public class MusicGroupImpl implements MusicGroup {

    private final Map<String, Integer> albums = new HashMap<>();
    private final Set<Song> songs = new HashSet<>();

    @Override
    public void addAlbum(final String albumName, final int year) {
        this.albums.put(albumName, year);
    }

    @Override
    public void addSong(final String songName, final Optional<String> albumName, final double duration) {
        if (albumName.isPresent() && !this.albums.containsKey(albumName.get())) {
            throw new IllegalArgumentException("invalid album name");
        }
        this.songs.add(new MusicGroupImpl.Song(songName, albumName, duration));
    }

    @Override
    public Stream<String> orderedSongNames() {
    	Stream<String> s = songs.stream().sorted((x,y)-> x.getSongName().compareTo(y.getSongName())).map(x->""+x.getSongName());
        return  s;
        }

    @Override
    public Stream<String> albumNames() {
        Stream<String> s1 = albums.entrySet().stream().map(x -> "" + x.getKey());
    	return s1;
    }

    @Override
    public Stream<String> albumInYear(final int year) {
        Stream<String> s2 = albums.entrySet().stream().filter(x-> x.getValue().equals(year)).map(x->x.getKey());
        
    	return s2;
    }

    @Override
    public int countSongs(final String albumName) {
    	
    	
    	long i = songs.stream().filter(x-> x.getAlbumName().isPresent() && x.getAlbumName().get().equals(albumName)).count();
        return (int)i;
    }

    @Override
    public int countSongsInNoAlbum() {
        long s = this.songs.stream().filter(x->x.getAlbumName().isPresent() == false).count();
    	return (int)s;
    }

    @Override
    public OptionalDouble averageDurationOfSongs(final String albumName) {
       double l = 0;
       int c = 0;
    	for(Song s : songs){
    		
    	   if(s.getAlbumName().isPresent() && s.getAlbumName().get().equals(albumName)){
    		   l+=s.getDuration();
    		   c++;  
    	   }
    	   
    	   
       }
    	l /=c;
    	/*
    	 songs.stream().filter(x-> x.getAlbumName().isPresent() && x.getAlbumName().get().equals(albumName))
    			.mapToDouble(x-> {
    				double l +=x.getDuration();
    				int c++;
    				return l/c;
    			}).forEach(System.out::println);		
    		
    	*/
    	return  OptionalDouble.of(l);
    }

    @Override
    public Optional<String> longestSong() {
    	Optional<String> s5 = songs.stream().max((x,y)-> {return (int) (x.getDuration()-y.getDuration());}).map(x-> "" + x.getSongName());
    	//Optional<String> s5 = songs.stream().collect(maxBy((x,y)->(int) (x.getDuration()-y.getDuration()))).map(x-> "" + x.getSongName());
      return s5;
    }

    @Override
    public Optional<String> longestAlbum() {
        
    	Optional<String> s6 = Optional.empty();
    	double lung = 0;
    	double max = 0;
    	for(String s : albums.keySet()){
    		lung = 0;
    		for(Song s1 : songs){
    			
    			if(s1.getAlbumName().isPresent() && s1.getAlbumName().get().equals(s) ){
    				lung+= s1.getDuration();
    				
    			}
    			if(max < lung){ 
    				max = lung;
    			s6 = Optional.of(s);}
    		}
    		
    	}
    	
    	
    	return s6;
    }

    private static final class Song {

        private final String songName;
        private final Optional<String> albumName;
        private final double duration;
        private int hash;

        private Song(final String name, final Optional<String> album, final double len) {
            super();
            this.songName = name;
            this.albumName = album;
            this.duration = len;
        }

        public String getSongName() {
            return songName;
        }

        public Optional<String> getAlbumName() {
            return albumName;
        }

        public double getDuration() {
            return duration;
        }

        @Override
        public int hashCode() {
            if (hash == 0) {
                hash = songName.hashCode() ^ albumName.hashCode() ^ Double.hashCode(duration);
            }
            return hash;
        }

        @Override
        public boolean equals(final Object obj) {
            if (obj instanceof Song) {
                final Song other = (Song) obj;
                return albumName.equals(other.albumName) && songName.equals(other.songName)
                        && duration == other.duration;
            }
            return false;
        }

        @Override
        public String toString() {
            return "Song [songName=" + songName + ", albumName=" + albumName + ", duration=" + duration + "]";
        }

    }

}
